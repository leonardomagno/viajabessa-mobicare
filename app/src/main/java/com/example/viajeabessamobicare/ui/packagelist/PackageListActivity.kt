package com.example.viajeabessamobicare.ui.packagelist

import android.app.AlertDialog
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.viajeabessamobicare.R
import com.example.viajeabessamobicare.base.BaseListAdapterInterface
import com.example.viajeabessamobicare.gone
import com.example.viajeabessamobicare.models.enums.ViewStatus
import com.example.viajeabessamobicare.ui.PackageDetailsActivity
import com.example.viajeabessamobicare.visible

class PackageListActivity : AppCompatActivity(), BaseListAdapterInterface.OnItemClickListener,
    View.OnClickListener {

    private lateinit var loadingView: View

    private val travelPackageListAdapter = PackageListAdapter()

    val TRAVEL_PACKAGE = "TRAVEL_PACKAGE"

    private lateinit var travelPackageListViewModel: TravelPackageListViewModel

    private val cityId = "rio01" //Parâmetros para chamada da lista de repositórios

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_package_list)

        travelPackageListViewModel = ViewModelProvider(this).get(TravelPackageListViewModel::class.java)

        setupView()
        applyObserver()
        travelPackageListViewModel.travelPackageList(cityId = cityId)
    }

    /**
     * Função que configura o Recycler View e view de loading.
     */
    private fun setupView() {
        changeStatusBarColor()

        travelPackageListAdapter.itemClickListener = this

        loadingView = findViewById(R.id.loading_view_package_list)

        findViewById<RecyclerView>(R.id.activity_package_list_recycler_view)?.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = travelPackageListAdapter
        }
    }

    override fun onItemClick(position: Int, view: View) {
        val clickedItem = travelPackageListAdapter.currentList.getOrNull(position)

        val intent = Intent(this, PackageDetailsActivity::class.java)
        intent.putExtra(TRAVEL_PACKAGE, clickedItem)
        startActivity(intent)
    }

    override fun onClick(p0: View?) {
    }

    private fun applyObserver() {
        travelPackageListViewModel.liveDataPackageList.observe(this, Observer { viewData ->
            when (viewData.viewStatus) {
                ViewStatus.SUCCESS -> {
                    loadingView.gone()
                    val list = viewData.data
                    if (!list.isNullOrEmpty()) travelPackageListAdapter.submitList(list)
                }
                ViewStatus.LOADING -> {
                    loadingView.visible()
                }
                ViewStatus.ERROR -> {
                    loadingView.gone()
                    val builder = dialogBuilder(
                        message = R.string.activity_package_list_alert_dialog_error_message,
                        positiveLabel = R.string.activity_package_list_alert_dialog_positive_action_label,
                        positiveAction = {
                            val dialog: AlertDialog? = null
                            dialog?.dismiss()
                            finish()
                        }
                    )
                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                }
            }
        })
    }

    /**
     * Função que constrói a dialog.
     */
    private fun dialogBuilder(
        message: Int,
        positiveLabel: Int,
        positiveAction: () -> Unit = {}
    ): AlertDialog.Builder {

        return AlertDialog.Builder(this).apply {
            setMessage(message)
            setPositiveButton(positiveLabel) { _, _ ->
                positiveAction.invoke()
            }
        }
    }

    /**
     * Função que muda a cor da status bar para verde.
     */
    private fun changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= 21) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = this.resources.getColor(R.color.green)
        }
    }
}