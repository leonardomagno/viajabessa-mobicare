package com.example.viajeabessamobicare.ui

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.example.viajeabessamobicare.R
import com.example.viajeabessamobicare.gone
import com.example.viajeabessamobicare.models.TravelPackageVO

class PackageDetailsActivity : AppCompatActivity(), View.OnClickListener {

    var travelPackageVO: TravelPackageVO? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_package_details)

        travelPackageVO =
            intent.getSerializableExtra("TRAVEL_PACKAGE") as TravelPackageVO

        setupView()
    }

    private fun setupView() {
        changeStatusBarColor()

        val buyButton = findViewById<Button>(R.id.activity_package_details_button_buy)

        val destinationPhoto = findViewById<ImageView>(R.id.activity_package_details_image_view_photo_detail)
        val destination = findViewById<TextView>(R.id.activity_package_details_text_view_destination)
        val price = findViewById<TextView>(R.id.activity_package_details_text_view_price)
        val description = findViewById<TextView>(R.id.activity_package_details_text_view_description)
        val rating = findViewById<ImageView>(R.id.activity_package_details_image_view_rating)
        val date = findViewById<TextView>(R.id.activity_package_details_text_view_date)
        val persons = findViewById<TextView>(R.id.activity_package_details_text_view_persons)
        val wifi = findViewById<TextView>(R.id.activity_package_details_text_view_convenience_wifi)
        val food = findViewById<TextView>(R.id.activity_package_details_text_view_convenience_food)
        val visits = findViewById<TextView>(R.id.activity_package_details_text_view_convenience_car)
        val accessibility = findViewById<TextView>(R.id.activity_package_details_text_view_convenience_accessibility)

        Glide.with(this).load(travelPackageVO?.featurePicture).into(destinationPhoto)
        destination.text = travelPackageVO?.tourDestination
        price.text = travelPackageVO?.tourSuggestedPrice.toString()
        description.text = travelPackageVO?.tourDescription
        date.text = travelPackageVO?.tourDate
        persons.text = travelPackageVO?.tourPersons

        setupRating(travelPackageVO, rating)

        if (!travelPackageVO?.tourWifi!!) wifi.gone()
        if (!travelPackageVO?.tourFood!!) food.gone()
        if (!travelPackageVO?.tourVisits!!) visits.gone()
        if (!travelPackageVO?.tourAccessibility!!) accessibility.gone()

        buyButton.setOnClickListener(this)

    }

    private fun changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= 21) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = this.resources.getColor(R.color.green)
        }
    }

    private fun setupRating(travelPackageVO: TravelPackageVO?, imageView: ImageView) {
        val drawableToLoad = when (travelPackageVO?.tourRating) {
            2 -> R.drawable.ic_two_stars_rating
            3 -> R.drawable.ic_three_stars_rating
            4 -> R.drawable.ic_four_stars_rating
            5 -> R.drawable.ic_five_stars_rating
            else -> R.drawable.ic_one_star_rating
        }

        imageView.setImageResource(drawableToLoad)
    }

    override fun onClick(view: View?) {
        Toast.makeText(this,"Compra realizada", Toast.LENGTH_LONG).show()
    }

}