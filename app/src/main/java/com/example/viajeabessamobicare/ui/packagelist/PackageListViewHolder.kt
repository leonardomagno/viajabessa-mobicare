package com.example.viajeabessamobicare.ui.packagelist

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.viajeabessamobicare.R
import com.example.viajeabessamobicare.base.BaseListAdapterInterface
import com.example.viajeabessamobicare.gone
import com.example.viajeabessamobicare.models.TravelPackageVO

class PackageListViewHolder(
    private val context: Context,
    view: View,
    private val itemClickListener: BaseListAdapterInterface.OnItemClickListener?
) :
    RecyclerView.ViewHolder(view), View.OnClickListener {

    init {
        itemView.setOnClickListener(this)
    }

    // Views
    private val viewHolderPackageImageViewRating =
        itemView.findViewById<ImageView>(R.id.view_holder_package_image_view_rating)
    private val viewHolderPackageTextViewDestination =
        itemView.findViewById<TextView>(R.id.view_holder_package_text_view_destination)
    private val viewHolderPackageTextViewPrice =
        itemView.findViewById<TextView>(R.id.view_holder_package_text_view_price)
    private val viewHolderPackageTextViewDuration =
        itemView.findViewById<TextView>(R.id.view_holder_package_text_view_duration)
    private val viewHolderPackageImageViewIcon =
        itemView.findViewById<ImageView>(R.id.view_holder_package_image_view_icon)
    private val viewHolderPackageImageViewWifi =
        itemView.findViewById<ImageView>(R.id.view_holder_package_image_view_wifi_icon)
    private val viewHolderPackageImageViewFood =
        itemView.findViewById<ImageView>(R.id.view_holder_package_image_view_food)
    private val viewHolderPackageImageViewCar =
        itemView.findViewById<ImageView>(R.id.view_holder_package_image_view_car_icon)
    private val viewHolderPackageImageViewAccessibility =
        itemView.findViewById<ImageView>(R.id.view_holder_package_image_view_wheelchair)

    init {
        itemView.findViewById<View>(R.id.view_holder_package_list_click_overlay)
            ?.setOnClickListener(this)
    }


    override fun onClick(viewClickedOn: View?) {
        viewClickedOn?.let {
            itemClickListener?.onItemClick(adapterPosition, viewClickedOn)
        }
    }

    fun bind(item: TravelPackageVO) {
        viewHolderPackageTextViewDestination.text = item.tourDestination
        viewHolderPackageTextViewPrice.text = item.tourSuggestedPrice.toString()
        viewHolderPackageTextViewDuration.text = item.tourDuration
        Glide.with(context).load(item.featurePicture).into(viewHolderPackageImageViewIcon)

        //Lógica que altera o número de estrelas de acordo com o valor do tourRating.
        val drawableToLoad = when (item.tourRating) {
            2 -> R.drawable.ic_two_stars_rating
            3 -> R.drawable.ic_three_stars_rating
            4 -> R.drawable.ic_four_stars_rating
            5 -> R.drawable.ic_five_stars_rating
            else -> R.drawable.ic_one_star_rating
        }

        viewHolderPackageImageViewRating.setImageResource(drawableToLoad)

        //Controle de visibilida dos ícones de comodidades de acordo com o valor boleano.
        if (!item.tourWifi) {
            viewHolderPackageImageViewWifi.gone()
        }

        if (!item.tourFood) {
            viewHolderPackageImageViewFood.gone()
        }

        if (!item.tourVisits) {
            viewHolderPackageImageViewCar.gone()
        }

        if (!item.tourAccessibility) {
            viewHolderPackageImageViewAccessibility.gone()
        }
    }
}