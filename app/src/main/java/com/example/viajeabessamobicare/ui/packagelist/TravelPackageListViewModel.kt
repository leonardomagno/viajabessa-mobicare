package com.example.viajeabessamobicare.ui.packagelist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.viajeabessamobicare.models.TravelPackageVO
import com.example.viajeabessamobicare.models.ViewData
import com.example.viajeabessamobicare.models.enums.ViewStatus
import com.example.viajeabessamobicare.repositories.TravelPackageRepository

class TravelPackageListViewModel: ViewModel() {

    val liveDataPackageList = MutableLiveData<ViewData<List<TravelPackageVO>>>()

    private val repository = TravelPackageRepository

    /**
     * Função que faz a requisição da lista de pacotes de viagem via camada de repositório..
     */
    fun travelPackageList(cityId: String) {

        liveDataPackageList.value = ViewData(viewStatus = ViewStatus.LOADING)
        repository.getTravelPackagetList(cityId = cityId,
            onSucess = {
                liveDataPackageList.value = ViewData(data = it, viewStatus = ViewStatus.SUCCESS)
            },
            onError = {
                liveDataPackageList.value = ViewData(viewStatus = ViewStatus.ERROR)
            }

        )

    }
}