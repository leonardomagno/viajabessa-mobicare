package com.example.viajeabessamobicare.ui.packagelist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.viajeabessamobicare.R
import com.example.viajeabessamobicare.base.BaseListAdapterInterface
import com.example.viajeabessamobicare.models.TravelPackageVO

class PackageListAdapter : ListAdapter<TravelPackageVO, RecyclerView.ViewHolder>(
    object : DiffUtil.ItemCallback<TravelPackageVO>() {

        /**
         * Função compara itens do adapter a fim de determinar se os itens recém introduzimos representam
         * novos itens ou atualizações de itens já existentes no adapter.
         *
         * @param oldItem Item previamente existente no Adapter
         * @param newItem Item introduzido ao Adapter pela submissão de nova lista de itens
         *
         */
        override fun areItemsTheSame(oldItem: TravelPackageVO, newItem: TravelPackageVO) =
            oldItem.tourId == newItem.tourId

        override fun areContentsTheSame(oldItem: TravelPackageVO, newItem: TravelPackageVO) =
            oldItem == newItem
    }) {


    var itemClickListener: BaseListAdapterInterface.OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PackageListViewHolder(
            parent.context,
            LayoutInflater.from(parent.context).inflate(
                R.layout.view_holder_package_list,
                parent,
                false
            ), itemClickListener
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position)?.let {
            (holder as? PackageListViewHolder)?.bind(it)
        }
    }
}