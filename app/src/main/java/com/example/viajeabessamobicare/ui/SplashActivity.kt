package com.example.viajeabessamobicare.ui

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.example.viajeabessamobicare.ui.packagelist.PackageListActivity
import com.example.viajeabessamobicare.R


class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        changeStatusBarColor()
        splashTransition()
    }

    fun splashTransition() {
        val intent = Intent(this@SplashActivity, PackageListActivity::class.java)

        Handler().postDelayed({
            startActivity(intent)
            finish()
        }, 3000)
    }

    fun changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= 21) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = this.resources.getColor(R.color.green)
        }
    }
}