package com.example.viajeabessamobicare.base

import android.view.View

interface BaseListAdapterInterface {

    interface OnItemClickListener {
        fun onItemClick(position: Int, view: View)
    }

}