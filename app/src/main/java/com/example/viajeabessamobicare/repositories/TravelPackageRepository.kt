package com.example.viajeabessamobicare.repositories

import com.example.viajeabessamobicare.models.TravelPackageDTO
import com.example.viajeabessamobicare.models.TravelPackageResponse
import com.example.viajeabessamobicare.models.TravelPackageVO
import com.example.viajeabessamobicare.repositories.api.TravelPackageAPI
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object TravelPackageRepository {

    private const val API_URL = "https://private-d5468-viajeabessa3.apiary-mock.com/"
    private val travelPackageAPI: TravelPackageAPI

    /**
     * inicialização do Retrofit com a URL da API hospedada no Apiary.
     */
    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        travelPackageAPI = retrofit.create(TravelPackageAPI::class.java)
    }

    /**
     * Função que requisita a lista de pacotes de viagem.
     */
    fun getTravelPackagetList(
        cityId: String,
        onSucess: (TravelPackageList: List<TravelPackageVO>) -> Unit,
        onError: () -> Unit
    ) {
        travelPackageAPI.getTravelPackageList(cityId)
            .enqueue(object : Callback<TravelPackageResponse> {
                override fun onResponse(
                    call: Call<TravelPackageResponse>,
                    response: Response<TravelPackageResponse>
                ) {
                    if (response.isSuccessful) {
                        val userResponse = response.body()
                        if (userResponse != null) {
                            onSucess.invoke(transformTravelPackageVOList(userResponse.data))
                        } else {
                            onError()
                        }
                    } else {
                        onError()
                    }
                }

                override fun onFailure(call: Call<TravelPackageResponse>, t: Throwable) {
                    onError()
                }
            })
    }

    /**
     * Função que transformam o objeto DTO em um  objeto VO.
     */
    private fun transformTravelPackageVOList(data: List<TravelPackageDTO>): List<TravelPackageVO> =
        data.map { travelPackageDTO -> travelpackageToTravelPackageVO(travelPackageDTO) }


    private fun travelpackageToTravelPackageVO(dto: TravelPackageDTO): TravelPackageVO =
        TravelPackageVO(
            tourId = dto.tourId,
            tourDestination = dto.tourDestination,
            tourSuggestedPrice = dto.tourSuggestedPrice,
            tourDescription = dto.tourDescription,
            tourRating = dto.tourRating,
            tourDuration = dto.tourDuration,
            tourDate = dto.tourDate,
            tourPersons = dto.tourPersons,
            tourWifi = dto.tourWifi,
            tourVisits = dto.tourVisits,
            tourFood = dto.tourFood,
            tourAccessibility = dto.tourAccessibility,
            featurePicture = dto.featurePicture
        )
}