package com.example.viajeabessamobicare.repositories.api

import com.example.viajeabessamobicare.models.TravelPackageResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface TravelPackageAPI {

    @GET("tours")
    fun getTravelPackageList(
        @Query("cityId") cityId: String
    ): retrofit2.Call<TravelPackageResponse>
}