package com.example.viajeabessamobicare.models

import com.example.viajeabessamobicare.models.enums.ViewStatus

data class ViewData<D> (
    val data: D? = null,
    val viewStatus: ViewStatus
)