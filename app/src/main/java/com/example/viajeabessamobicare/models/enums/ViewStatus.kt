package com.example.viajeabessamobicare.models.enums

enum class ViewStatus {
    SUCCESS,
    EMPTY_RESULT,
    LOADING,
    ERROR
}