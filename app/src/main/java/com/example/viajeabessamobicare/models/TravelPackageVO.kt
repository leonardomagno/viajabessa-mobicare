package com.example.viajeabessamobicare.models

import java.io.Serializable

data class TravelPackageVO (
    val tourId: String? = null,
    val tourDestination: String? = null,
    val tourSuggestedPrice: String? = null,
    val tourDescription: String? = null,
    val tourRating: Int? = null,
    val tourDuration: String? = null,
    val tourDate: String? = null,
    val tourPersons: String? = null,
    val tourWifi: Boolean = false,
    val tourVisits: Boolean = false,
    val tourFood: Boolean = false,
    val tourAccessibility: Boolean = false,
    val featurePicture: String? = null
) : Serializable