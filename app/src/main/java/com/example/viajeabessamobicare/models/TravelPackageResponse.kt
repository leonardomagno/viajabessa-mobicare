package com.example.viajeabessamobicare.models

data class TravelPackageResponse(
    val totalResults: Int,
    val data: List<TravelPackageDTO>
)