package com.example.viajeabessamobicare.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TravelPackageDTO (
    @SerializedName("tourId") val tourId: String,
    @SerializedName("tourDestination") val tourDestination: String,
    @SerializedName("tourSuggestedPrice") val tourSuggestedPrice: String,
    @SerializedName("tourRating") val tourRating: Int,
    @SerializedName("tourDescription") val tourDescription: String,
    @SerializedName("tourDuration") val tourDuration: String? = null,
    @SerializedName("tourDate") val tourDate: String? = null,
    @SerializedName("tourPersons") val tourPersons: String? = null,
    @SerializedName("tourWifi") val tourWifi: Boolean = false,
    @SerializedName("tourVisits") val tourVisits: Boolean = false,
    @SerializedName("tourFood") val tourFood: Boolean = false,
    @SerializedName("tourAccessibility") val tourAccessibility: Boolean = false,
    @SerializedName("featurePicture") val featurePicture: String
): Parcelable